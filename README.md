I'm a student at the Poznań University of Technology.

# Programing languages
## Favorite 

<img src="https://upload.wikimedia.org/wikipedia/commons/9/99/Unofficial_JavaScript_logo_2.svg" alt="JavaScript" width="200" height="200"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/1/18/ISO_C%2B%2B_Logo.svg" alt="C++" width="200" height="200"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/8/87/Sql_data_base_with_logo.png" alt="SQL" width="200" />


## Fine to work with
<img src="https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg" alt="Python" width="200" height="200"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/6/61/HTML5_logo_and_wordmark.svg" alt="HTML" width="200" height="200" />
<img src="https://upload.wikimedia.org/wikipedia/commons/d/d5/CSS3_logo_and_wordmark.svg" alt="CSS" width="200" height="200"/>

# Interesting projects

## [Destination West](https://gitlab.com/niche-devs/destination-west)
### Overall
This project is developed by the group "Niche Devs". Our goal was to create a western game without using game engine. We chosed JavaScript because it's easy to learn and most of us were familiar with it. In this project we learned how to use Git to control our project version, how to work in a big project with many people, how to discuss new features that we would like to develop. Good communication was needed to develop multiple parts of the project independendly and later on join them together.

### My part
My part was to create the display of a town. We chosed to create the town from presets.  The layout of the city will change depending on the overall number of buildings and the number of buildings of each type. This generator places important buildings (like a Sheriff's Office or a saloon) in the center, to provide the feeling of a western town. Due to lack of graphics we could never use the full potential of this code and we had to display only one type of a building everywhere.

## [Pacman](https://gitlab.com/gn-put-projects/pacman-3d)
### Overall
This project was created using OpenGL. It's a 3d pacman game. The Maze, the placment of crystals and behaviour of the ghosts is similar to orginal game release in 1980. This project was created with [Artur Gulik](https://gitlab.com/ArturGulik).

### My part
In this project I had to create an AI for every ghost compliant with the original game. It means that every ghost has a scatter mode,a frighten mode and a chase mode. There were a lot of different smaller parts of code that I had to develop like the shaders or ghost teleportation.

## [Labyrinth](https://gitlab.com/gn-put-projects/arduino-labyrinth-game)
### Overall
This project was created using Arduino. Player moves in the maze by using joystick. The size of the maze depends on the difficulty. The maze is generated randomly so every game is different. After finding the exit you can see your score. This project was created with [Artur Gulik](https://gitlab.com/ArturGulik).

### My part
In this project I had to create random maze generation. To do so I used the hunt and kill algorithm. This algorithm has two modes. The first one is the kill mode. In this mode the algorithm creates a path, by moving to not visited places. This stage persists as long as we can move in any direction. If this isn't posible then the algorithm goes into hunt mode. In this mode we are looking for a not visited place to start hunt mode again. Moreover I had to prepare periferials, so my friend wouldn't have to worry about wires and could focus on typing code in C++.
